
rm -rf ./icons-md-react/src/action
rm -rf ./icons-md-react/src/alert
rm -rf ./icons-md-react/src/av
rm -rf ./icons-md-react/src/communication
rm -rf ./icons-md-react/src/content
rm -rf ./icons-md-react/src/device
rm -rf ./icons-md-react/src/editor
rm -rf ./icons-md-react/src/file
rm -rf ./icons-md-react/src/hardware
rm -rf ./icons-md-react/src/image
rm -rf ./icons-md-react/src/maps
rm -rf ./icons-md-react/src/navigation
rm -rf ./icons-md-react/src/notification
rm -rf ./icons-md-react/src/places
rm -rf ./icons-md-react/src/social
rm -rf ./icons-md-react/src/toggle
cp -r ./distLex/react/* ./icons-md-react/src

cd ./icons-md-react
npm i
npm run build
