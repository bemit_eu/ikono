# Icons and Icons and Formats

Copies, renames and enhances the SVG Icons.

Currently handles Material Design Icons, planned is Font Awesome.

> Official Material Design Icons Source: [google/material-design-icons](https://github.com/google/material-design-icons)

## Usage

Download Repo with submodules.

Extract SVG and lex the icons into different usage formats:

```bash
php rename.php
```

Generated folders are:

```
/dist - Plain SVG
/distLex - root for different formats
/distLex/react - react components
/distLex/twig - twig components
```

## Licence

The source for the icons generator/parser is under the [Apache Licence Version 2.0](LICENCE).

Icons are licenced under different licences, see their original licenses before usage.

### Material Design Icons License

We have made these icons available for you to incorporate into your products under the Apache License Version 2.0. Feel free to remix and re-share these icons and documentation in your products. We'd love attribution in your app's about screen, but it's not required. The only thing we ask is that you not re-sell these icons.

[Licence](https://github.com/google/material-design-icons#license)

### Font Awesome Licence

Font Awesome Free is free, open source, and GPL friendly. You can use it for commercial projects, open source projects, or really almost whatever you want.

    Icons — CC BY 4.0 License
        In the Font Awesome Free download, the CC BY 4.0 license applies to all icons packaged as .svg and .js files types.
    Fonts — SIL OFL 1.1 License
        In the Font Awesome Free download, the SIL OLF license applies to all icons packaged as web and desktop font files.
    Code — MIT License
        In the Font Awesome Free download, the MIT license applies to all non-font and non-icon files.

Attribution is required by MIT, SIL OLF, and CC BY licenses. Downloaded Font Awesome Free files already contain embedded comments with sufficient attribution, so you shouldn't need to do anything additional when using these files normally.

We've kept attribution comments terse, so we ask that you do not actively work to remove them from files, especially code. They're a great way for folks to learn about Font Awesome.

[Licence](https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
