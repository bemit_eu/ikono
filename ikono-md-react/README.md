# Material Icons for different Usages

Copies, renames and enhances the SVG Material Design Icons.

> Official Icons Source: [google/material-design-icons](https://github.com/google/material-design-icons)

## Usage

Download Repo including submodules.

Extract SVG and lex the icons into different usage formats:

```bash
php rename.php
```

Generated folders are:

```
/dist - Plain SVG
/distLex - root for different formats
/distLex/react - react components
/distLex/twig - twig components
```

## Licence

Like the icons also this source is under the [Apache Licence Version 2.0](LICENCE).

### Icons License

We have made these icons available for you to incorporate into your products under the Apache License Version 2.0. Feel free to remix and re-share these icons and documentation in your products. We'd love attribution in your app's about screen, but it's not required. The only thing we ask is that you not re-sell these icons.

[Icons Licence](https://github.com/google/material-design-icons#license)
