import React, {Component} from 'react'
import {render} from 'react-dom'

import * as Icons from '../../src'

class Demo extends Component {
    render() {
        return <div>
            <h1>icons-md-react Demo</h1>
            {Object.keys(Icons).map(Icon => {
                const Ic = Icons[Icon];
                return <Ic/>
            })}
        </div>
    }
}

render(<Demo/>, document.querySelector('#demo'));
