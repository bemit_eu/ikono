<?php

function cleanDir($dir) {
    if(!is_dir($dir)) {
        return;
    }
    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
    foreach($files as $file) {
        if($file->isDir()) {
            rmdir($file->getRealPath());
        } else {
            unlink($file->getRealPath());
        }
    }
    rmdir($dir);
}

function mvIcons($folder, $dist, $lvl = 0) {
    $rii = new RecursiveDirectoryIterator($folder, RecursiveDirectoryIterator::SKIP_DOTS);
    foreach($rii as $file) {
        /**
         * @var \FilesystemIterator $file
         */
        if($file->isFile()) {
            continue;
        }
        if($lvl === 0) {
            if(!is_dir($file->getPathname() . '/svg/production')) {
                continue;
            }
            error_log('Start Category ' . $file->getBasename());
            cpIconsFiles($file->getPathname() . '/svg/production', $dist . '/' . $file->getBasename());
            error_log('End Category ' . $file->getBasename());
        }
    }
}

function cpIconsFiles($from, $to) {
    $rii = new RecursiveDirectoryIterator($from, RecursiveDirectoryIterator::SKIP_DOTS);
    foreach($rii as $file) {
        /**
         * @var \FilesystemIterator $file
         */
        if($file->isDir()) {
            continue;
        }
        if(strpos($file->getBasename(), '_24px')) {
            $clean_to = str_replace(['_24px', 'ic_'], ['', ''], $file->getBasename());
            error_log('  Movin File `' . $file->getBasename() . '` to `' . $clean_to . '`');
            if(!is_dir($to)) {
                mkdir($to, 0744, true);
            }
            copy($file->getPathname(), $to . '/' . $clean_to);
        }
    }
}

function lexIcons($folder, $to, $lexer, $lexer_fin) {
    $rii = new RecursiveDirectoryIterator($folder, RecursiveDirectoryIterator::SKIP_DOTS);
    $parsed_all = [];
    foreach($rii as $file) {
        /**
         * @var \FilesystemIterator $file
         */
        $category = $file->getBasename();
        if(!$file->isDir()) {
            continue;
        }
        $riii = new RecursiveDirectoryIterator($file->getPathname(), RecursiveDirectoryIterator::SKIP_DOTS);
        foreach($riii as $fil) {
            /**
             * @var \FilesystemIterator $fil
             */
            $contents = file_get_contents($fil->getPathname());
            foreach($lexer as $lex_name => $lex) {
                if(!is_dir($to . '/' . $lex_name . '/' . $category)) {
                    mkdir($to . '/' . $lex_name . '/' . $category, 0777, true);
                }
                $name = str_replace('.svg', '', $fil->getBasename());
                $parsed = $lex($name, $contents);

                $parsed_all[$lex_name][$parsed['name']] = $category . '/' . $parsed['name'];
                file_put_contents($to . '/' . $lex_name . '/' . $category . '/' . $parsed['name'], $parsed['contents']);

                error_log('Parsed File ' . $parsed['name']);
            }
        }
    }

    foreach($parsed_all as $lex_name => $names) {
        if(isset($lexer_fin[$lex_name])) {
            $lexer_fin[$lex_name]($names, $to . '/' . $lex_name);
        }
    }
}

cleanDir(__DIR__ . '/dist');
cleanDir(__DIR__ . '/distLex');
mvIcons(__DIR__ . '/ic-material-design', __DIR__ . '/dist');
lexIcons(__DIR__ . '/dist', __DIR__ . '/distLex', [
    'react' => static function($name, $contents) {
        $contents = str_replace(
            [
                'width="24"',
                'height="24"',
                // using viewBox will add a property just before the `viewBox` property, must append `viewBox` in replace also, could be used n-times
                'viewBox',
                'viewBox',
                'viewBox',
            ],
            [
                'width={props.size || \'24\'}',
                'height={props.size || \'24\'}',
                'fill={props.fill || \'#000000\'} viewBox',
                'style={props.style} viewBox',
                'className={props.className} viewBox',
            ],
            $contents
        );

        $n = '';
        foreach(explode('_', $name) as $nn) {
            $n .= ucfirst($nn);
        }

        $tpl = <<<TPL
import React from 'react';

const Ic{$n} = (props) => {
    return {$contents};
};

export {Ic{$n}};
TPL;

        return [
            'name' => 'Ic' . $n . '.js',
            'contents' => $tpl,
        ];
    },
    'twig' => static function($name, $contents) {
        $n = '';
        foreach(explode('_', $name) as $nn) {
            $n .= ucfirst($nn);
        }
        return [
            'name' => $n . '.twig',
            'contents' => $contents,
        ];
    },
], [
    'react' => static function($names, $dist) {
        $imports = '';
        foreach($names as $name => $path) {
            $imports .= 'export {' . str_replace('.js', '', $name) . '} from "./' . $path . '";';
        }

        file_put_contents($dist . '/index.js', $imports);
        error_log('Finalized React, totally exporting `' . count($names) . '` icons: ' . $dist . '/index.js');
    },
]);
